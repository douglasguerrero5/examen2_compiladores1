#ifndef _AST_H
#define _AST_H

#include <iostream>
#include <string>
#include <memory>
#include <list>

using namespace std;

class Expr;
class Stmt;

using PExpr = std::unique_ptr<Expr>;
using ExprList = std::list<PExpr>;
using PStmt = std::unique_ptr<Stmt>;
using StmtList = std::list<PStmt>;

class ASTNode {
public:
    ASTNode() {}
    virtual ~ASTNode() {}
};

class Expr: public ASTNode {
public:
    virtual int eval(Context& ctx) = 0;
};

class NumExpr : public ASTNode{
    public:
        int value;
        NumExpr(int val){
            value = val;
        }
};

class IDExpr : public ASTNode{
    public:
        std::string value;
        IDExpr(std::string val){
            value = val;
        }
};

class AssignStmt : public ASTNode{
    public:
        AssignStmt(std::string var, PExpr expresion1) : var(std::move(var)){}
        std::string var;
};

class AddExpr: public Expr {
    public:
        AddExpr(std::unique_ptr<NumExpr> expresion1, std::unique_ptr<NumExpr> expresion2) : expresion1(std::move(expresion1)), expresion2(std::move(expresion2)) {}
        std::unique_ptr<NumExpr> expresion1;
        std::unique_ptr<NumExpr> expresion2;
};

class SubExpr: public Expr {
    public:
        SubExpr(std::unique_ptr<NumExpr> expresion1, std::unique_ptr<NumExpr> expresion2) : expresion1(std::move(expresion1)), expresion2(std::move(expresion2)) {}
        std::unique_ptr<NumExpr> expresion1;
        std::unique_ptr<NumExpr> expresion2;
};

class MulExpr: public Expr {
    public:
        MulExpr(std::unique_ptr<NumExpr> expresion1, std::unique_ptr<NumExpr> expresion2) : expresion1(std::move(expresion1)), expresion2(std::move(expresion2)) {}
        std::unique_ptr<NumExpr> expresion1;
        std::unique_ptr<NumExpr> expresion2;
};

class DivExpr: public Expr {
    public:
        DivExpr(std::unique_ptr<NumExpr> expresion1, std::unique_ptr<NumExpr> expresion2) : expresion1(std::move(expresion1)), expresion2(std::move(expresion2)) {}
        std::unique_ptr<NumExpr> expresion1;
        std::unique_ptr<NumExpr> expresion2;
};

class ModExpr: public Expr {
    public:
        ModExpr(std::unique_ptr<NumExpr> expresion1, std::unique_ptr<NumExpr> expresion2) : expresion1(std::move(expresion1)), expresion2(std::move(expresion2)) {}
        std::unique_ptr<NumExpr> expresion1;
        std::unique_ptr<NumExpr> expresion2;
};

class Stmt: public ASTNode {
public:
    virtual void exec(Context& ctx) = 0;
};


#endif
